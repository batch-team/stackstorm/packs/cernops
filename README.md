# CERNOps Pack for StackStorm

Provides the common integration functionality needed to make stackstorm fly in CERN infra.

## Configuration

Refer to `config.schema.yaml` for configuration schema

## Installation

Since the pack is not listed/there isn't a CERN StackStorm Exchange, we need to manually install the pack from git

> st2 pack install <https://gitlab.cern.ch/batch-team/stackstorm/packs/cernops.git>

### Puppet

We can use the `st2::packs` setting in our `puppet-st2` module to manage packs and their configs.

#### Via YAML (Heira)
```
# data/hostgroup/playground/mystackstorm.yaml
st2::packs:
  cernops:
    ensure: present
    version: 1.0.0
    config:
      base_url: "batchstorm.cern.ch"
      default_chatops_channel: "chatops-test"
      gitlab_url: gitlab.cern.ch
      gitlab_packs_base: batch-team/stackstorm/packs
      gitlab_username: {{ st2kv('system.gitlab_password') }}
      gitlab_password: {{ st2kv('system.gitlab_password', decrypt=true) }}
```

## StackStorm Pack Contents

### Actions

### Sensors

### Triggers

### Rules

#### Notify

Sets up the default (mattermost) based notification channel

#### Notify Inquiry

Sets up the notification (mattermost) for inquiries

#### AlertManager webhook

This consumes a standard webhook at `/v1/webhooks/alertmanager` and dumps the payload using `core.echo`. It serves as an example for our alertmanager integration.

### Policies
